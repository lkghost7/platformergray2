﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ExitDoor : MonoBehaviour{
    [SerializeField] [Tooltip("номер сцены")]
    private int buildIndex;

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.transform.CompareTag("Player")) {
            LoadLevel(buildIndex);
        }
    }

    private void LoadLevel(int buildIndex) {
        SceneManager.LoadScene(buildIndex);
    }
}
