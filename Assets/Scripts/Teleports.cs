﻿using UnityEngine;

public class Teleports : MonoBehaviour
{

    [SerializeField][Tooltip("второй телепорт")] private GameObject teleport2;
    
    private void OnTriggerEnter2D(Collider2D other) {
    if(other.gameObject.transform.CompareTag("Player")) {
            other.transform.position = teleport2.transform.position;
        }
    }

 
}
